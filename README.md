# Gestycontrol extended FPDF library
## Credits
Original library by http://www.fpdf.org/ . All credits to them.
Library is extended by Gestycontrol to add commonly used functions and multi-format lines.

## Usage
Extend the Fpdf/ExtendedPdf class for each PDF you're creating. Use the additional functions provided with `$this->function()`;
           
- `SetMarginTop`: Sets the tMargin property and moves cursor.
- `SetMarginRight`: Sets the rMargin property and moves cursor.
- `SetMarginBottom`: Sets the bMargin property and moves cursor.
- `SetMarginLeft`: Sets the lMargin property and moves cursor.
- `GetLineHeight`: Gets a calculated line height for current font.
- `InlineMultiCell`: Creates multiple inline cells changing line when required continuing after the last cell. Call it
    multiple times while changing font to generate a multi-format line.  
- `Ln`: Adds a new line. If not set, the height is calculated using `$this->getLineHeight()`; 
- `SetStyle`: Sets only the font style.
- `SetFontFamily`: Sets only the font family.
- `OpenLi`: Opens li.
- `CloseLi`: Closes li.
- `OpenBorder`: Opens border.
- `CloseBorder`: Closes border.