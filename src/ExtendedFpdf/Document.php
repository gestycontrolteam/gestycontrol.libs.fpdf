<?php

namespace ExtendedFpdf;

use InvalidArgumentException;
use function chr;

/**
 * Class Document
 *
 * @package ExtendedFpdf
 */
class Document extends \Fpdf\Fpdf {
    /**
     * Lists.
     *
     * @var array
     */
    protected $lis = array();

    /**
     * Border statuses.
     *
     * @var array
     */
    protected $borders = array();

    /**
     * @var string
     */
    protected $FontStyle = '';

    /**
     * @param int $tMargin
     */
    public function SetMarginTop($tMargin) {
        $this->tMargin = $tMargin;

        if ($this->y < $this->tMargin) {
            $this->y = $this->tMargin;
        }
    }

    /**
     * @param int $rMargin
     */
    public function SetMarginRight($rMargin) {
        $this->rMargin = $rMargin;

        if ($this->x > $this->w - $this->rMargin - $this->lMargin) {
            $this->Ln();
            $this->x = $this->lMargin;
        }
    }

    /**
     * @param int $bMargin
     */
    public function SetMarginBottom($bMargin) {
        $this->bMargin = $bMargin;

        if ($this->y + $this->GetStringWidth('M') * 1.5 > $this->h - $this->bMargin - $this->tMargin) {
            $this->AddPage();
            $this->y = $this->tMargin;
            $this->x = $this->lMargin;
        }
    }

    /**
     * @param int $lMargin
     */
    public function SetMarginLeft($lMargin) {
        $this->lMargin = $lMargin;

        if ($this->x < $this->lMargin) {
            $this->x = $this->lMargin;
        }
    }

    /**
     * Line height
     *
     * @return float
     */
    protected function GetLineHeight() {
        return $this->GetStringWidth('M') * 1.5;
    }

    /**
     * ExtendedPdf constructor.
     *
     * @param string $orientation
     * @param string $unit
     * @param string $size
     * @param string $margin CSS style without units
     */
    public function __construct($orientation = 'P', $unit = 'mm', $size = 'letter', $margin = '0 0 0 0') {
        parent::__construct($orientation, $unit, $size);

        if (!empty($margin)) {
            $marginSides = explode(' ', $margin);

            if (sizeof($marginSides) > 4) {
                throw new InvalidArgumentException('Margin must be a string with 1 to 4 integer values separated by single space.');
            }

            if (sizeof($marginSides) > 0) {
                $this->tMargin = intval($marginSides[0]);
            }

            if (sizeof($marginSides) > 1) {
                $this->rMargin = intval($marginSides[1]);
            } else {
                $this->rMargin = $this->tMargin;
            }

            if (sizeof($marginSides) > 2) {
                $this->bMargin = intval($marginSides[2]);
            } else {
                $this->bMargin = $this->tMargin;
            }

            if (sizeof($marginSides) > 3) {
                $this->lMargin = intval($marginSides[3]);
            } else {
                $this->lMargin = $this->rMargin;
            }
        } else {
            $this->tMargin = 0;
            $this->rMargin = 0;
            $this->bMargin = 0;
            $this->lMargin = 0;
        }
    }

    public function InlineMultiCell($txt = '', $link = '', $font_family = null, $font_style = null, $font_size = null) {
        $this->SetFont($font_family, $font_style, $font_size);
        $txt = str_replace(array("\r", "\t"), array('', ' '), $txt);

        while (strpos($txt, '  ') !== false) {
            $txt = str_replace('  ', ' ', $txt);
        }

        $lines = explode("\n", $txt);

        if (sizeof($lines) > 1) {
            foreach ($lines as $line) {
                $this->InlineMultiCell($line, $link);
                $this->Ln();
            }

            return;
        }

        $txt = $lines[0];
        $h = $this->GetLineHeight();

        if ($this->x <= 0) {
            $this->x = $this->lMargin;
        }

        $remaining_with = $this->w - $this->rMargin - $this->x;

        $words = explode(' ', $txt);
        $overflown = false;
        $includedWords = array();
        $excludedWords = array();

        foreach ($words as $word) {
            if (!$overflown) {
                $includedWords[] = $word;

                if ($this->GetStringWidth(implode(' ', $includedWords)) > $remaining_with) {
                    $overflown = true;

                    // Only split if it is not the first word of the line
                    if (sizeof($includedWords) > 1 || $this->x != $this->lMargin) {
                        $excludedWords[] = $word;
                        unset($includedWords[sizeof($includedWords) - 1]);
                    }
                }
            } else {
                $excludedWords[] = $word;
            }
        }

        if ($includedWords) {
            $this->Cell($this->GetStringWidth(implode(' ', $includedWords)), $h, implode(' ', $includedWords), 0, 0, '', false, $link);
        }

        if ($overflown) {
            $this->Ln();
            $this->x = $this->lMargin;

            if (strlen(trim(implode(' ', $excludedWords)))) {
                $this->InlineMultiCell(ltrim(implode(' ', $excludedWords)));
            }
        }
    }

    /**
     * New line
     *
     * @param float $h
     * @param int $times
     */
    public function Ln($h = null, $times = 1) {
        if ($h === null) {
            $h = $this->GetLineHeight();
        }

        if ($times > 1) {
            $this->Ln($h, $times - 1);
        }

        parent::Ln($h);
    }

    /**
     * @param string $style
     */
    public function SetStyle($style = '') {
        $this->SetFont(null, $style);
    }

    /**
     * @param string $font_family
     */
    public function SetFontFamily($font_family = null) {
        $this->SetFont($font_family);
    }

    /**
     * @param string $family
     * @param string $style
     * @param int $size
     */
    public function SetFont($family = null, $style = null, $size = null) {
        if ($family === null) {
            $family = $this->FontFamily;
        }

        if ($style === null) {
            $style = $this->FontStyle;
        }

        if ($size === null) {
            $size = $this->FontSizePt;
        }

        parent::SetFont($family, $style, $size);
    }

    /**
     * Opens a list item.
     *
     * @param string $bullet char
     * @param int $indent
     */
    function OpenLi($bullet = null, $indent = 5) {
        if ($bullet === null) {
            $bullet = chr(149);
        }

        if ($this->x != $this->lMargin) {
            $this->Ln();
        }

        $li = array('x' => $this->x);
        $this->setMarginLeft($li['x'] + $indent);
        $this->x = $this->lMargin;
        $this->inlineMultiCell($bullet);
        $this->setMarginLeft($li['x'] + $indent * 2);
        $this->x = $this->lMargin;
        $this->lis[] = $li;
    }

    /**
     * Closes last open list item.
     */
    function CloseLi() {
        $li = array_pop($this->lis);
        $this->setMarginLeft($li['x']);
        $this->Ln();
    }

    /**
     * Opens a border
     */
    function OpenBorder() {
        if ($this->x != $this->lMargin) {
            $this->Ln();
        }

        $border = array('y' => $this->y);
        $this->borders[] = $border;
    }

    /**
     * Closes the border
     * @param int $width
     * @param int $padding
     */
    function CloseBorder($width = 1, $padding = 5) {
        $border = array_pop($this->borders);
        $current_x = $this->x;
        $current_y = $this->y;

        $this->SetXY($this->lMargin - $padding, $border['y'] - $padding);
        $this->Cell($this->w - $this->lMargin - $this->rMargin + $padding * 2, $current_y - $border['y'] + $padding * 2, '', $width);
        $this->SetXY($current_x, $current_y);
        $this->Ln();
    }

    /**
     * Adds a cell. Decodes UTF-8.
     *
     * @param $w
     * @param int $h
     * @param string $txt
     * @param int $border
     * @param int $ln
     * @param string $align
     * @param bool $fill
     * @param string $link
     */
    public function Cell($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = false, $link = '') {
        if (mb_detect_encoding($txt) == 'UTF-8') {
            $txt = str_replace('€', '[[EURO-]]', $txt);
            $txt = utf8_decode($txt);
            $txt = str_replace('[[EURO-]]', chr(128), $txt);
        }

        parent::Cell($w, $h, $txt, $border, $ln, $align, $fill, $link);
    }
}